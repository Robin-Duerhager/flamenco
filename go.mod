module projects.blender.org/studio/flamenco

go 1.20

require (
	github.com/adrg/xdg v0.4.0
	github.com/benbjohnson/clock v1.3.0
	github.com/deepmap/oapi-codegen v1.9.0
	github.com/disintegration/imaging v1.6.2
	github.com/dop251/goja v0.0.0-20230812105242-81d76064690d
	github.com/dop251/goja_nodejs v0.0.0-20211022123610-8dd9abb0616d
	github.com/fromkeith/gossdp v0.0.0-20180102154144-1b2c43f6886e
	github.com/gertd/go-pluralize v0.2.1
	github.com/getkin/kin-openapi v0.88.0
	github.com/glebarez/go-sqlite v1.21.1
	github.com/glebarez/sqlite v1.8.0
	github.com/golang/mock v1.6.0
	github.com/google/shlex v0.0.0-20191202100458-e7afc7fbc510
	github.com/google/uuid v1.3.0
	github.com/graarh/golang-socketio v0.0.0-20170510162725-2c44953b9b5f
	github.com/labstack/echo/v4 v4.9.1
	github.com/mattn/go-colorable v0.1.12
	github.com/pkg/browser v0.0.0-20210911075715-681adbf594b8
	github.com/rs/zerolog v1.26.1
	github.com/stretchr/testify v1.7.0
	github.com/ziflex/lecho/v3 v3.1.0
	golang.org/x/crypto v0.0.0-20211215165025-cf75a172585e
	golang.org/x/image v0.5.0
	golang.org/x/net v0.7.0
	gopkg.in/yaml.v2 v2.4.0
	gorm.io/gorm v1.25.2
	modernc.org/sqlite v1.23.1
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dlclark/regexp2 v1.7.0 // indirect
	github.com/dustin/go-humanize v1.0.1 // indirect
	github.com/ghodss/yaml v1.0.0 // indirect
	github.com/go-openapi/jsonpointer v0.19.5 // indirect
	github.com/go-openapi/swag v0.19.5 // indirect
	github.com/go-sourcemap/sourcemap v2.1.3+incompatible // indirect
	github.com/golang-jwt/jwt v3.2.2+incompatible // indirect
	github.com/google/pprof v0.0.0-20230207041349-798e818bf904 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/kballard/go-shellquote v0.0.0-20180428030007-95032a82bc51 // indirect
	github.com/labstack/gommon v0.4.0 // indirect
	github.com/mailru/easyjson v0.7.0 // indirect
	github.com/mattn/go-isatty v0.0.17 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/remyoudompheng/bigfft v0.0.0-20230129092748-24d4a6f8daec // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasttemplate v1.2.1 // indirect
	golang.org/x/mod v0.8.0 // indirect
	golang.org/x/sys v0.5.0 // indirect
	golang.org/x/text v0.7.0 // indirect
	golang.org/x/time v0.0.0-20210220033141-f8bda1e9f3ba // indirect
	golang.org/x/tools v0.6.1-0.20230217175706-3102dad5faf9 // indirect
	golang.org/x/vuln v0.0.0-20230320232729-bfc1eaef17a4 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
	lukechampine.com/uint128 v1.2.0 // indirect
	modernc.org/cc/v3 v3.40.0 // indirect
	modernc.org/ccgo/v3 v3.16.13 // indirect
	modernc.org/libc v1.22.5 // indirect
	modernc.org/mathutil v1.5.0 // indirect
	modernc.org/memory v1.5.0 // indirect
	modernc.org/opt v0.1.3 // indirect
	modernc.org/strutil v1.1.3 // indirect
	modernc.org/token v1.0.1 // indirect
)
